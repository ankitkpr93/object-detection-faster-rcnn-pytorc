#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np # linear algebra
import json
from matplotlib import pyplot as plt
from skimage import color
from skimage.feature import hog
from sklearn import svm
from sklearn.metrics import classification_report,accuracy_score
import cv2, os
from skimage import feature
from sklearn import metrics
import seaborn as sns


# In[ ]:



# In[4]:


with_payload_images = os.listdir("payload_dataset/With_payload_cropped_images/")
without_payload_images = os.listdir("payload_dataset/Without_payload_cropped_images/")


# In[5]:


# In[6]:


labelnames=[' (without_payload)',' (with_payload)']
print(len(with_payload_images),len(without_payload_images))
labels_with = [1] * len(with_payload_images)
labels_without = [0] * len(without_payload_images)
labels = labels_with + labels_without
print(len(labels))


# In[7]:


data = []
size = (60,30)


# In[9]:


for each in with_payload_images:
    img = cv2.imread('payload_dataset/With_payload_cropped_images/%s'%each,cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img,size)
    data.append(img)
for each in without_payload_images:
    img = cv2.imread('payload_dataset/Without_payload_cropped_images/%s'%each,cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img,size)
    data.append(img)


# In[10]:


data = np.array(data)
#data.shape


# In[11]:


# view few images and print its corresponding label
img_index = 10
fig = plt.figure()
ax1 = fig.add_subplot(2,2,1)
ax1.axis('off')
ax1.imshow(data[img_index],cmap='gray')

ax2 = fig.add_subplot(2,2,2)
ax2.axis('off')
img_index = 1000
ax2.imshow(data[img_index],cmap='gray')

ax2 = fig.add_subplot(2,2,3)
ax2.axis('off')
img_index = 2000
ax2.imshow(data[img_index],cmap='gray')

ax2 = fig.add_subplot(2,2,4)
ax2.axis('off')
img_index = 3000
ax2.imshow(data[img_index],cmap='gray')


# In[9]:


hog_features = []
for img_index in range(len(data)):
    # load the image, and extract HOG features
    image = (data[img_index])
    #gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    H = feature.hog(image, orientations=9, pixels_per_cell=(10, 10),
                  cells_per_block=(2, 2), transform_sqrt=True, block_norm="L2-Hys")

    # update the data and labels
    hog_features.append(H)
print(np.shape(hog_features))


# In[10]:


labels =  np.array(labels).reshape(len(labels),1)
hog_features = np.array(hog_features)
data_frame = np.hstack((hog_features,labels))
np.random.shuffle(data_frame)
percentage = 80
partition = int(len(hog_features)*percentage/100)
x_train, x_test = data_frame[:partition,:-1],  data_frame[partition:,:-1]
y_train, y_test = data_frame[:partition,-1:].ravel() , data_frame[partition:,-1:].ravel()

#clf.fit(x_train,y_train)
model = svm.SVC(kernel='rbf',C=100.0, random_state=42,gamma='auto',verbose=True) # rbf Kernel
model.fit(x_train,y_train)


# In[11]:


print("Train set Accuracy: {:.2f}".format(model.score(x_train,y_train)))


# In[12]:


y_pred = model.predict(x_test)
accuracy = accuracy_score(y_test, y_pred)
print("Test Accuracy: "+str(accuracy_score(y_test, y_pred)))
print('\n')
print(classification_report(y_test, y_pred))


# In[13]:


# plot the confusion matrix
cm  = metrics.confusion_matrix(y_test, y_pred)
print ("Confusion matrix")
print(cm)

# Plot confusion matrix using seaborn library
plt.figure(figsize=(16,9))
sns.heatmap(cm, annot=True, fmt=".3f", linewidths=.5, square = True, cmap = 'Blues_r');
plt.ylabel('Actual label');
plt.xlabel('Predicted label');
all_sample_title = 'Test Accuracy Score: {0}'.format(accuracy)
plt.title(all_sample_title, size = 15);


# In[14]:


import joblib
filename = 'SVM_drone_model.sav'
joblib.dump(model, filename)
 
# some time later...
 
# load the model from disk
model = joblib.load(filename)
#result = loaded_model.score(X_test, Y_test)
#print(result)


# In[19]:


images = []
orig_labels=[]
# randomly select a few testing fashion items
for i in np.random.choice(np.arange(500, len(labels)), size=(16,)):
    # classify the clothing
    test_img = (data[i])
    H = feature.hog(test_img, orientations=9, pixels_per_cell=(10, 10),
                  cells_per_block=(2, 2), transform_sqrt=True, block_norm="L2-Hys")

    pred = model.predict(H.reshape(1, -1))[0]
    #prediction = model.predict(test_img.reshape(1, -1))
    label = labelnames[int(pred)]
    orig_labels.append(labelnames[int(labels[i])])
    color = (0, 255, 0)
    test_img = cv2.merge([test_img] * 3)
    test_img = cv2.resize(test_img, (60, 30), interpolation=cv2.INTER_LINEAR)
    cv2.putText(test_img, label, (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.50, color, 2)
    images.append(test_img)


# In[20]:


fig = plt.figure(figsize=(16,9))



ax1 = fig.add_subplot(2,4,1)
ax1.imshow(images[1])
print(orig_labels[1])
ax2 = fig.add_subplot(2,4,2)
ax2.imshow(images[2])
print(orig_labels[2])
ax3 = fig.add_subplot(2,4,3)
ax3.imshow(images[3])
print(orig_labels[3])
ax4 = fig.add_subplot(2,4,4)
ax4.imshow(images[4])
print(orig_labels[4])
plt.figure(figsize=(16,9))


ax5 = fig.add_subplot(1,4,1)
ax5.imshow(images[5])
print(orig_labels[5])
ax6 = fig.add_subplot(1,4,2)
ax6.imshow(images[6])
print(orig_labels[6])
ax7 = fig.add_subplot(1,4,3)
ax7.imshow(images[7])
print(orig_labels[7])
ax8 = fig.add_subplot(1,4,4)
ax8.imshow(images[8])
print(orig_labels[8])


# In[ ]:




