import random, cv2, torch, torchvision, os, time
from model_instance import get_model_instance
import pandas as pd
import numpy as np
import argparse
from nms import non_max_suppression_fast

def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example: \n python inference.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --video_path videos/2019-09-25_1535.mp4 --score_threshold 0.8')
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store")
    parser.add_argument('--score_threshold', action="store", default=0.9, type=float)
    parser.add_argument('--checkpoint_path', action="store")
    parser.add_argument('--video_path', action="store")
    args = parser.parse_args()
    return args

def inference_video(dataset_path, model_name, checkpoint_path, score_threshold, video_path):
    random.seed(101)
    color_scheme={}
    dataframe = pd.read_csv(os.path.join(dataset_path,'labels.csv'))
    classes = list(dataframe['class'].unique())
    classes.insert(0,'background')
    for each_class in classes:
        r,g,b = random.randint(0,255),random.randint(0,255),random.randint(0,255)
        color_scheme[each_class] = (r,g,b)


    num_classes = len(classes)
    model = get_model_instance(model_name, num_classes)
    checkpoint = torch.load(checkpoint_path)
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)
    cap = cv2.VideoCapture(video_path)
    count = 0
    total_time = 0
    ret = True
    while(ret):
        # Capture frame-by-frame
        torch.cuda.synchronize()
        tic = time.time()
        ret, frame = cap.read()
        if not ret:
            break
        count +=1
        frame = cv2.resize(frame,(1280,720))

        prediction = []
        with torch.no_grad():
            transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
            image = transform(frame)

            prediction = model([image.to(device)])[0]
            #print (prediction)
            length = len(prediction['scores'].tolist())
            boxes = []
            for i in range (length):

                score = str(round(prediction['scores'].tolist()[i],2))
                if float(score) > score_threshold:
                    label = classes[prediction['labels'].tolist()[i]]
                    if label.startswith('drone'):
                        boxes.append(prediction['boxes'].tolist()[i])
                        box = non_max_suppression_fast(np.array(boxes),0.8)[0]
                        #box = [(int(boxes[0])), (int(boxes[1])), (int(boxes[2])), (int(boxes[3]))]
                        frame = cv2.rectangle(frame,(box[0],box[1]),(box[2],box[3]),color_scheme[label] ,2)
                        frame = cv2.putText(frame,label+" "+str(score),(box[0],box[1]-10),cv2.FONT_HERSHEY_SIMPLEX,0.7,color_scheme[label],2)
        cv2.imshow('frame',frame)
        toc = time.time()
        total_time += (toc-tic)
        if cv2.waitKey(1) & 0xFF == ord('\x1b'):
            break
    cap.release()
    cv2.destroyAllWindows()

    # When everything done, release the capture
    print ("fps =  %s"%(1/(total_time/count)))

def main():
    args = argument_parser()
    inference_video(args.dataset_path,args.model_name,args.checkpoint_path,args.score_threshold,args.video_path)

if __name__== "__main__":
  main()
