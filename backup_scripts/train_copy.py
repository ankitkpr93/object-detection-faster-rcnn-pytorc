import torchvision, torch, os
from model_instance import get_model_instance
import dataset_preparation
from dataset_preparation import dataset_prep
from vision.references.detection.engine import train_one_epoch
from vision.references.detection import utils
import argparse


def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example: \n python train.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --epochs 10 --batch_size 8 --lr 0.001 --optimizer sgd' )
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store")
    parser.add_argument('--batch_size', action="store", default=8, type=int)
    parser.add_argument('--epochs', action="store", default=10, type=int)
    parser.add_argument('--checkpoint_path', action="store")
    parser.add_argument('--lr', default=0.001, type=float)
    parser.add_argument('--optimizer', action="store", default='sgd')
    args = parser.parse_args()
    return args


def train_model(model,data_loader,checkpoint_path,num_epochs, num_classes,model_name,lr,opt):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]
    if opt=='sgd':
        optimizer = torch.optim.SGD(params, lr=lr,
                                momentum=0.9, weight_decay=0.0005)
    if opt=='adam':
        optimizer = torch.optim.Adam(params, lr=lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0.0005, amsgrad=False)

    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.1)

    start_epoch = 1
    if checkpoint_path:
      checkpoint = torch.load(checkpoint_path)
      start_epoch = checkpoint['epoch']
      model.load_state_dict(checkpoint['state_dict'])
      optimizer.load_state_dict(checkpoint['optimizer'])
      # now individually transfer the optimizer parts...
      for state in optimizer.state.values():
          for k, v in state.items():
              if isinstance(v, torch.Tensor):
                  state[k] = v.to(device)
      model.train()


    for epoch in range(max(1,start_epoch),num_epochs):

        # train for one epoch, printing every 10 iterations
        train_one_epoch(model, optimizer, data_loader, device, epoch, print_freq=10)
        #saving checkpoint
        #torch.save(model,checkpoint_dir+'/%s_epoch_%s.pth'%(model_name,epoch))
        # update the learning rate
        lr_scheduler.step()
        # evaluate on the test dataset
        #evaluate(model, data_loader_test, device=device)
        state = {'epoch': epoch + 1, 'state_dict': model.state_dict(),
                 'optimizer': optimizer.state_dict(), 'classes': num_classes,}
        torch.save(state, 'checkpoints/drone_%s_optimizer_%s_classes_%s_epoch_%s.pth'%(model_name,opt,num_classes,epoch))


def main():
    args = argument_parser()
    data_loader, data_loader_test, num_classes = dataset_prep(args.dataset_path,args.batch_size)[:-2]
    model = get_model_instance(args.model_name, num_classes)
    train_model(model,data_loader,args.checkpoint_path,args.epochs+1,num_classes,args.model_name,args.lr,args.optimizer)

if __name__== "__main__":
  main()
