import torchvision, torch, os
from model_instance import get_model_instance
from dataset_preparation import dataset_prep
from vision.references.detection.engine import train_one_epoch
from vision.references.detection import utils
import argparse
import plotly, math
import plotly.graph_objs as go
import numpy as np
from xmls_to_csv import xml_to_csv
from classes import fetch_classes



#parses all the inline arguments
def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example: \n python train.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_optimizer_sgd_classes_15_epoch_1.pth --epochs 10 --batch_size 8 --lr 0.001 --optimizer sgd ' )
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store")
    parser.add_argument('--batch_size', action="store", default=8, type=int)
    parser.add_argument('--epochs', action="store", default=10, type=int)
    parser.add_argument('--checkpoint_path', action="store")
    parser.add_argument('--lr', default=0.001, type=float)
    parser.add_argument('--optimizer', action="store", default='sgd')
    args = parser.parse_args()
    return args

#Draws interactive plots using plotlt library and saves the plots in graphs directory.
def draw_plot(*lists):

    train_loss = go.Scatter(
        mode = "lines+markers",
        name =  "Training loss",
        x = np.arange(1,len(lists[0])+1),
        y = lists[0],
        marker = {"size": 8}
    )
    data = [train_loss]
    if len(lists)>1:
        val_loss = go.Scatter(
            mode = "lines+markers",
            name = "Validation loss",
            x = np.arange(1,len(lists[1])+1),
            y = lists[1],
            marker = {"size": 8}
        )
        data = [train_loss, val_loss]
    layout = go.Layout(
        autosize=True,
        width=1500,
        height=900,
        title = 'Validation and Training Loss',
        titlefont=dict(size=50),
        yaxis=dict(
            showticklabels=True,
            title = "Loss",
            tickfont=dict(
                family='Old Standard TT, serif',
                size=22,
                color='black'
            )),
        xaxis=dict(
            showticklabels=True,
            title = "Epoch/Iterations",
            tickfont=dict(
                family='Old Standard TT, serif',
                size=22,
                color='black'
            )
        )
    )
    fig = go.Figure(data=data,layout=layout)
    return fig



#computes validation loss for the whole validation set
def compute_val_loss(model,data_loader_test,epoch):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)
    test_count = 0
    val_total_loss = 0
    val_loss_classifier = 0
    val_loss_box_reg = 0
    val_loss_rpn_box_reg = 0
    val_loss_objectness = 0
    for images, targets in data_loader_test:
        test_count += 1
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
        with torch.no_grad():
            val_loss_dict = model(images, targets)
            val_loss_classifier += val_loss_dict['loss_classifier'].tolist()
            val_loss_box_reg += val_loss_dict['loss_box_reg'].tolist()
            val_loss_rpn_box_reg += val_loss_dict['loss_rpn_box_reg'].tolist()
            val_loss_objectness += val_loss_dict['loss_objectness'].tolist()
            val_losses = sum(loss for loss in val_loss_dict.values()).tolist()
            val_total_loss += val_losses
    print ('\nValidation loss after Epoch %s\n\tloss_classifier : %s\n\tloss_box_reg : %s\n\tloss_rpn_box_reg : %s\n\tloss_objectness : %s\n\ttotal_loss : %s\n'%(epoch, round(val_loss_classifier/test_count,5), round(val_loss_box_reg/test_count,5), round(val_loss_rpn_box_reg/test_count,5), round(val_loss_objectness/test_count,5), round(val_total_loss/test_count,5)))
    return round(val_total_loss/test_count,5)

#trains the model using selected architecture and saves the checkpoints in the checkpoints directory
def train_model(model,data_loader,data_loader_val,checkpoint_path,num_epochs, num_classes ,model_name, lr, opt, batch_size):
    val_loss_list = []
    train_loss_list = []
    loss_list = []  #train_loss_list for each iteration

    #checks the available device
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]
    if opt=='sgd':
        optimizer = torch.optim.SGD(params, lr=lr,
                                momentum=0.9, weight_decay=0.0005)
    if opt=='adam':
        optimizer = torch.optim.Adam(params, lr=lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0.0005, amsgrad=False)

    #construct a lr scheduler
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.9)

    #Loading checkpoint
    start_epoch = 1
    if checkpoint_path:
      checkpoint = torch.load(checkpoint_path)
      start_epoch = checkpoint['epoch']
      model.load_state_dict(checkpoint['state_dict'])
      if 'optimizer' in checkpoint.keys(): optimizer.load_state_dict(checkpoint['optimizer'])
      if 'classes' in checkpoint.keys(): print('%s\tClasses found in the checkpoint.\n'%checkpoint['classes'])
      if 'validation_loss_list' in checkpoint.keys(): val_loss_list = checkpoint['validation_loss_list']
      if 'train_loss_list' in checkpoint.keys(): train_loss_list = checkpoint['train_loss_list']
      # now individually transfer the optimizer parts...
      for state in optimizer.state.values():
          for k, v in state.items():
              if isinstance(v, torch.Tensor):
                  state[k] = v.to(device)


    #itearting through number of epochs
    for epoch in range(max(1,start_epoch),num_epochs):
        count = 0
        total_loss = 0
        loss_classifier = 0
        loss_box_reg = 0
        loss_rpn_box_reg = 0
        loss_objectness = 0
        #iterating through each batch in data loader
        for images, targets in data_loader:
            count += 1
            images = list(image.to(device) for image in images)
            size = len(images)
            targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
            loss_dict = model(images, targets)  #computing the losses
            losses = sum(loss for loss in loss_dict.values())
            loss_classifier += loss_dict['loss_classifier'].tolist()
            loss_box_reg += loss_dict['loss_box_reg'].tolist()
            loss_rpn_box_reg += loss_dict['loss_rpn_box_reg'].tolist()
            loss_objectness += loss_dict['loss_objectness'].tolist()
            total_loss += losses.tolist()
            loss_list.append(round(total_loss/count,3))
            optimizer.zero_grad()
            losses.backward() #Backpropagation
            optimizer.step()

            #plotting the training loss for every 10th iteration and printing the losses
            if count%10==0:
                fig = draw_plot(loss_list)
                plotly.offline.plot(fig, filename = 'graphs/training_loss_curve_%s.html'%(model_name),auto_open=False,image_height=720,image_width=1280)
                print ('Epoch [%s/%s] (%s/%s) :\n lr : %s\t||\tloss_classifier : %s\t||\tloss_box_reg : %s\t||\tloss_rpn_box_reg : %s\t||\tloss_objectness : %s\t||\ttotal_loss : %s\n'%(epoch, num_epochs-1, count, math.ceil(len(data_loader.dataset)/batch_size), round(optimizer.param_groups[0]["lr"],6), round(loss_classifier/count,5), round(loss_box_reg/count,5), round(loss_rpn_box_reg/count,5), round(loss_objectness/count,5), round(total_loss/count,5)))

        # update the learning rate
        lr_scheduler.step()

        # evaluate on the test dataset
        val_loss_list.append(compute_val_loss(model,data_loader_val,epoch))
        train_loss_list.append(round(total_loss/count,5))

        #plotting the training and validation loss for every epoch
        fig = draw_plot(train_loss_list, val_loss_list)
        plotly.offline.plot(fig, filename = 'graphs/loss_curve_%s.html'%(model_name),auto_open=False,image_height=720,image_width=1280)
        print ('graph plotted and saved in graphs folder.\n')

        #Saving the checkpoint
        state = {'epoch': epoch + 1, 'state_dict': model.state_dict(),
                 'optimizer': optimizer.state_dict(), 'num_classes': num_classes,
                 'train_loss_list' : train_loss_list, 'validation_loss_list' : val_loss_list,
                 'classes' : fetch_classes()}
        torch.save(state, 'checkpoints/drone_%s_optimizer_%s_classes_%s_epoch_%s.pth'%(model_name,opt,num_classes,epoch))
        print ('drone_%s_optimizer_%s_classes_%s_epoch_%s.pth saved in checkpoint path.\n'%(model_name,opt,num_classes,epoch))


def main():
    args = argument_parser()
    xml_to_csv(args.dataset_path)
    classes = fetch_classes()
    num_classes = len(classes)
    data_loader = dataset_prep(args.dataset_path,'train',classes,args.batch_size)
    data_loader_val = dataset_prep(args.dataset_path,'val',classes)
    print('\nThe number of training and validation samples are %s and %s respectively with %s classes\n. '%(len(data_loader.dataset),len(data_loader_val.dataset),num_classes))
    model = get_model_instance(args.model_name, num_classes)
    train_model(model,data_loader,data_loader_val,args.checkpoint_path,args.epochs+1,num_classes,args.model_name,args.lr,args.optimizer,args.batch_size)


if __name__== "__main__":
  main()
