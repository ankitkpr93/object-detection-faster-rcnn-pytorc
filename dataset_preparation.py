import torch, random, os
from vision.references.detection import utils
from dataset import *
from transform import get_transform
import pandas as pd
import numpy as np

def dataset_prep(root_folder,dataset_type,classes,train_batch_size=1):
    if dataset_type=='train':
        dataset = Dataset(root_folder, dataset_type, classes,get_transform(train=True))
    if dataset_type=='test':
        dataset = Dataset(root_folder,dataset_type, classes, get_transform(train=False))
    if dataset_type=='val':
        dataset = Dataset(root_folder,dataset_type, classes, get_transform(train=False))

    data_loader = torch.utils.data.DataLoader(
        dataset, batch_size=train_batch_size, shuffle=True, num_workers=4,
        collate_fn=utils.collate_fn)

    return data_loader
