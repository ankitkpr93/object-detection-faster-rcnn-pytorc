# Faster RCNN pytorch

**Detections**

![](output_1608.gif)

![](output_1608-2.gif)


**1. Requirements**


        1.  Pytorch (1.3.1) 
        2.  Torchvision (0.4.2)
        3.  CUDA 10.2
        4.  CUDNN 7
        5.  Plotly


**2. Models supported**

```
        1.  Resnet18_fpn
        2.  Resnet18
        2.  Resnet34_fpn
        3.  Resnet34
        3.  Resnet50_fpn
        4.  Resnet50
        4.  Resnet101_fpn
        5.  Resnet101
        5.  VGG16
        6.  Googlenet
        7.  Alexnet
        8.  Mobilenet
        9.  Densenet121
        10.  Inception (Soon to come...)

```


**3. Dataset Preparation**

To prepare the dataset, use Drone_data_preparation.ipynb 

Prepare the dataset in the PascalVOC format and keep it inside the dataset folder. The structure of the folder is shown below.

```

/dataset
        /VOC2007
                /Annotations
                        a.xml
                        b.xml
                          ....
                /JPEGImages
                        a.jpg
                        b.jpg
                        ....
                /ImageSets
                        /Main
                            trainval.txt
                            test.txt
                            train.txt
                labels.csv
      
```

To change the distribution of training, validation and test dataset, execute the following:

 `python sets.py --split 90 5 5`
    
#This will divide the dataset in 90:5:5 for training, validation and test respectively

**4. Jetson Xavier Pytorch setup**


    Compatibility
    
    PyTorch v1.0 - torchvision v0.2.2
    
    PyTorch v1.1 - torchvision v0.3.0
    
    PyTorch v1.2 - torchvision v0.4.0
    
    PyTorch v1.3 - torchvision v0.4.2
    
    PyTorch v1.4 - torchvision v0.5.0



***For Python 3.6***

1.  Install pip3 
    
    `sudo apt-get install python3-pip`
 
2. Installing Pytorch 1.3.0 
    
    `wget https://nvidia.box.com/shared/static/phqe92v26cbhqjohwtvxorrwnmrnfx1o.whl -O torch-1.3.0-cp36-cp36m-linux_aarch64.whl`

    `sudo apt-get install libopenblas-base`
    
    `pip3 install Cython`
    
    `pip3 install numpy torch-1.3.0-cp36-cp36m-linux_aarch64.whl`
 
3. Installing Torchvision 0.4.2 

    Before executing following command check the compatible version of torchvision from the table above.
 
    `sudo apt-get install libjpeg-dev zlib1g-dev`
 
    `git clone --branch v0.4.2  https://github.com/pytorch/vision torchvision`
    
    `cd torchvision`
    
    `sudo python3 setup.py install`
    
    `cd ../  `





***For Python 2.7***

1. Install pip

    `sudo apt-get install python-pip`

2. Installing Pytorch 1.3.0 
 
    `wget https://nvidia.box.com/shared/static/6t52xry4x2i634h1cfqvc9oaoqfzrcnq.whl -O torch-1.3.0-cp27-cp27mu-linux_aarch64.whl`

    `pip install torch-1.3.0-cp27-cp27mu-linux_aarch64.whl`

3. Installing Torchvision 0.4.2

    Before executing following command check the compatible version of torchvision from table above.
 
    `sudo apt-get install libjpeg-dev zlib1g-dev`

    `git clone --branch v0.4.2  https://github.com/pytorch/vision torchvision `
    
    `cd torchvision`
    
    `sudo python setup.py install`
    
    `cd ../  `


**5. Training**

`python train.py --model_name <model_name> --checkpoint_path <checkpoint_path> --epochs <number_of_epochs> --batch_size <value>`

    Example:
    python train.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --epochs 10 --batch_size 8


**6. Inference**

`python inference.py --model_name <model_name> --checkpoint_path <checkpoint_path> --video_path <video_path> --score_threshold <value>`

    Example:
    python inference.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --video_path videos/2019-09-25_1535.mp4 --score_threshold 0.8

**7. mAP Calculation**

`python mAP.py --model_name <model_name> --checkpoint_path <checkpoint_path> --score_threshold <value>`

    Example:
    python mAP.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --score_threshold 0.8


