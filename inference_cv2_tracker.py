import random, cv2, torch, torchvision, os, time
from model_instance import get_model_instance
import pandas as pd
import numpy as np
import argparse
from nms import non_max_suppression_fast
from classes import fetch_classes

def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example:\
     \n python inference.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --video_path videos/2019-09-25_1535.mp4 --score_threshold 0.9')
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store")
    parser.add_argument('--score_threshold', action="store", default=0.8, type=float)
    parser.add_argument('--checkpoint_path', action="store")
    parser.add_argument('--video_path', action="store")
    parser.add_argument("-t", "--tracker", type=str, default="csrt", help="OpenCV object tracker type")
    parser.add_argument("-dat", "--detection_interval", type=int, default=10, help="detection_interval")
    args = parser.parse_args()
    return args

def inference_video(dataset_path, model_name, checkpoint_path, score_threshold, video_path, tracker_type, detection_interval):
    random.seed(101)
    color_scheme={}
    classes = fetch_classes()
    detections_dict = {}
    for each_class in classes:
        if each_class.startswith('drone'):
            r,g,b = random.randint(0,255),random.randint(0,255),random.randint(0,255)
            color_scheme[each_class] = (r,g,b)
            detections_dict[each_class] = []

    OPENCV_OBJECT_TRACKERS = {
            "csrt": cv2.TrackerCSRT_create,
            "kcf": cv2.TrackerKCF_create,
            "boosting": cv2.TrackerBoosting_create,
            "mil": cv2.TrackerMIL_create,
            "tld": cv2.TrackerTLD_create,
            "medianflow": cv2.TrackerMedianFlow_create,
            "mosse": cv2.TrackerMOSSE_create
    }



    num_classes = len(classes)
    model = get_model_instance(model_name, num_classes)
    checkpoint = torch.load(checkpoint_path)
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)
    cap = cv2.VideoCapture(video_path)
    count = 0
    total_time = 0
    ret = True
    while(ret):
        # Capture frame-by-frame
        torch.cuda.synchronize()
        tic = time.time()
        ret, frame = cap.read()
        if not ret:
            break

        frame = cv2.resize(frame,(1280,720))
        #count +=1
        prediction = []
        with torch.no_grad():
            if count%detection_interval==0:


                transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
                image = transform(frame)

                prediction = model([image.to(device)])[0]


                length = len(prediction['scores'].tolist())
                for i in range (length):
                    score = str(round(prediction['scores'].tolist()[i],2))
                    if float(score) > score_threshold:
                        label = classes[prediction['labels'].tolist()[i]]
                        if label.startswith('drone'):
                            box = list(map(int, prediction['boxes'].tolist()[i]))
                            detections_dict[label].append(box)
                box_list = []
                for each_key in detections_dict.keys():
                    if len(detections_dict[each_key])>1:
                        detections_dict[each_key] = non_max_suppression_fast(np.array(detections_dict[each_key]),0.3)
                    for box in detections_dict[each_key]:
                        frame = cv2.rectangle(frame,(box[0],box[1]),(box[2],box[3]),color_scheme[each_key] ,2)
                        frame = cv2.putText(frame,each_key,(box[0],box[1]-10),cv2.FONT_HERSHEY_SIMPLEX,0.5,color_scheme[each_key],2)
                        box_list.append(box)
                    detections_dict[each_key]=[]
                trackers = cv2.MultiTracker_create()

                for each_box in box_list:
                    tracker = OPENCV_OBJECT_TRACKERS[tracker_type]()
                    trackers.add(tracker, frame, (each_box[0],each_box[1],each_box[2]-each_box[0],each_box[3]-each_box[1]))
            else:

                (success, boxes) = trackers.update(frame)
                boxes = non_max_suppression_fast(np.array(boxes),0.1)
                for box in boxes:
                    (x, y, w, h) = [int(v) for v in box]
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        cv2.imshow('frame',frame)
        count +=1
        toc = time.time()
        total_time += (toc-tic)
        if cv2.waitKey(1) & 0xFF == ord('\x1b'):
            break
    cap.release()
    cv2.destroyAllWindows()

    # When everything done, calculate the time
    print ("fps =  %s"%(1/(total_time/count)))

def main():
    args = argument_parser()
    inference_video(args.dataset_path,args.model_name,args.checkpoint_path,args.score_threshold,args.video_path,args.tracker,args.detection_interval)

if __name__== "__main__":
  main()
