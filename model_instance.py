from fpn import resnet_fpn_backbone
from torchvision.models.detection import FasterRCNN
import torchvision.models as models
from torchvision.models.detection.rpn import AnchorGenerator
import torchvision
import torch.nn as nn

def get_model_instance(model_name = 'vgg16',n_classes = 50):

    if model_name == 'resnet50_fpn':
        model = models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
        in_features = model.roi_heads.box_predictor.cls_score.in_features
        model.roi_heads.box_predictor = models.detection.faster_rcnn.FastRCNNPredictor(in_features,num_classes=n_classes)
        return model

    if model_name == 'resnet18_fpn':
        backbone = resnet_fpn_backbone('resnet18', True)
        model = FasterRCNN(backbone, num_classes=n_classes)
        return model

    if model_name == 'resnet34_fpn':
        backbone = resnet_fpn_backbone('resnet34', True)
        model = FasterRCNN(backbone, num_classes=n_classes)
        return model


    if model_name == 'resnet101_fpn':
        backbone = resnet_fpn_backbone('resnet101', True)
        model = FasterRCNN(backbone, num_classes=n_classes)
        return model


    if model_name == 'mobilenet':
        backbone = models.mobilenet_v2(pretrained=True).features
        backbone.out_channels = 1280

    if model_name == 'vgg16':
        backbone = models.vgg16(pretrained=True).features
        backbone.out_channels = 512

    if model_name == 'densenet':
        backbone = models.densenet121(pretrained=True).features
        backbone.out_channels = 1024

    if model_name == 'googlenet':
        backbone = models.googlenet(pretrained=True)
        modules = list(backbone.children())[:-1] # delete the last fc layer.
        backbone = nn.Sequential(*modules)
        backbone.out_channels = 1024

    if model_name == 'alexnet':
        backbone = models.alexnet(pretrained=True).features
        backbone.out_channels = 256

    '''if model_name == 'inception':
        backbone = models.inception_v3(pretrained=True)
        modules = list(backbone.children())[:-1] # delete the last fc layer.
        backbone = nn.Sequential(*modules)
        backbone.out_channels = 2048'''

    if model_name == 'resnet18':
        backbone = models.resnet18(pretrained=True)
        modules = list(backbone.children())[:-1] # delete the last fc layer.
        backbone = nn.Sequential(*modules)
        backbone.out_channels = 512

    if model_name == 'resnet34':
        backbone = models.resnet34(pretrained=True)
        modules = list(backbone.children())[:-1] # delete the last fc layer.
        backbone = nn.Sequential(*modules)
        backbone.out_channels = 512

    if model_name == 'resnet50':
        backbone = models.resnet50(pretrained=True)
        modules = list(backbone.children())[:-1] # delete the last fc layer.
        backbone = nn.Sequential(*modules)
        backbone.out_channels = 2048

    if model_name == 'resnet101':
        backbone = models.resnet101(pretrained=True)
        modules = list(backbone.children())[:-1] # delete the last fc layer.
        backbone = nn.Sequential(*modules)
        backbone.out_channels = 2048

    if model_name == 'resnet152':
        backbone = models.resnet152(pretrained=True)
        modules = list(backbone.children())[:-1] # delete the last fc layer.
        backbone = nn.Sequential(*modules)
        backbone.out_channels = 2048


    # FasterRCNN needs to know the number of
    # output channels in a backbone. For mobilenet_v2, it's 1280
    # so we need to add it here

    # let's make the RPN generate 5 x 3 anchors per spatial
    # location, with 5 different sizes and 3 different aspect
    # ratios. We have a Tuple[Tuple[int]] because each feature
    # map could potentially have different sizes and
    # aspect ratios
    anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                       aspect_ratios=((0.5, 1.0, 2.0),))

    # let's define what are the feature maps that we will
    # use to perform the region of interest cropping, as well as
    # the size of the crop after rescaling.
    # if your backbone returns a Tensor, featmap_names is expected to
    # be [0]. More generally, the backbone should return an
    # OrderedDict[Tensor], and in featmap_names you can choose which
    # feature maps to use.

    roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=[0],
                                                    output_size=7,
                                                    sampling_ratio=2)

    # put the pieces together inside a FasterRCNN model

    model = FasterRCNN(backbone,
                       num_classes=n_classes,
                       rpn_anchor_generator=anchor_generator,
                       box_roi_pool=roi_pooler)
    return model
