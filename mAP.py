from model_instance import get_model_instance
import torch, os
import pandas as pd
from dataset_preparation import dataset_prep
import argparse
from xmls_to_csv import xml_to_csv
from classes import fetch_classes


def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example: \n python mAP.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --score_threshold 0.8')
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store")
    parser.add_argument('--score_threshold', action="store", default=0.9, type=float)
    parser.add_argument('--checkpoint_path', action="store")
    parser.add_argument('--video_path', action="store")
    args = parser.parse_args()
    return args


def calculate_mAP(data_loader,checkpoint_path,model,generate_gt, generate_detections,score_threshold,classes):

    checkpoint = torch.load(checkpoint_path)
    model.load_state_dict(checkpoint['state_dict'])
    os.system('rm -f mAP/input/ground-truth/*.txt')
    os.system('rm -f mAP/input/detection-results/*.txt')
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)
    model.eval()
    prediction = []
    if generate_gt or generate_detections:
        for step, (img, target) in enumerate(data_loader):
            if generate_gt:#generate ground truth)
                no_of_objects = len(target[0]['labels'].tolist())
                file = open(r"mAP/input/ground-truth/%s.txt"%step,"w+")
                for each_object in range(no_of_objects):
                    label = classes[target[0]['labels'].tolist()[each_object]]
                    box = str(int(target[0]['boxes'].tolist()[each_object][0])) + " " + \
                    str(int(target[0]['boxes'].tolist()[each_object][1]))+ " " +\
                    str(int(target[0]['boxes'].tolist()[each_object][2]))+ " " +\
                    str(int(target[0]['boxes'].tolist()[each_object][3]))

                    file.write(label+" "+box+"\n")
                file.close()


            if generate_detections:#generate detection files

            # put the model in evaluation mode
                with torch.no_grad():
                    prediction = model([img[0].to(device)])[0]
                    no_of_pred_objects = len(prediction['labels'].tolist())
                    if no_of_pred_objects !=0:
                        file = open(r"mAP/input/detection-results/%s.txt"%step,"w+")
                        for each_object in range(no_of_pred_objects):
                            score = str(prediction['scores'].tolist()[each_object])
                            if float(score) > score_threshold:
                                label = classes[prediction['labels'].tolist()[each_object]]
                                box = str(int(prediction['boxes'].tolist()[each_object][0])) + " " + \
                                str(int(prediction['boxes'].tolist()[each_object][1]))+ " " +\
                                str(int(prediction['boxes'].tolist()[each_object][2]))+ " " +\
                                str(int(prediction['boxes'].tolist()[each_object][3]))
                                file.write(label+' '+score+' '+box+'\n')
                        file.close()
    os.system('find mAP/input/detection-results/ -type f -empty -delete')
    os.system('python mAP/scripts/extra/intersect-gt-and-dr.py')
    os.system('python mAP/main.py --ignore bee bird small-bird human vehicle quad-bike shadows background')

def main():
    args = argument_parser()
    xml_to_csv(args.dataset_path)
    classes = fetch_classes()
    num_classes = len(classes)
    data_loader_test = dataset_prep(args.dataset_path,'test',classes)
    model = get_model_instance(args.model_name, num_classes)
    calculate_mAP(data_loader_test,args.checkpoint_path,model,True,True,args.score_threshold,classes)

if __name__== "__main__":
  main()
