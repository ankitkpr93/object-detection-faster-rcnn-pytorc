import random, cv2, torch, torchvision, os, time
from model_instance import get_model_instance
import pandas as pd
import numpy as np
import argparse
from nms import non_max_suppression_fast
from classes import fetch_classes
#MN tracker
from os.path import join
import sys
sys.path.append("./trackers/SiamMask/")
sys.path.append(join("./trackers/SiamMask/", 'experiments', 'siammask_sharp'))
from trackers.SiamMask.tools.test import *
import torch
from types import SimpleNamespace
from trackers.SiamMask.utils.config_helper import load_config
from trackers.SiamMask.utils.load_helper import load_pretrain
from custom import Custom
import subprocess


def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example:\
     \n python inference.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_30.pth \
     --video_path videos/2019-09-25_1535.mp4 --score_threshold 0.9 --det_at 10')
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store")
    parser.add_argument('--score_threshold', action="store", default=0.8, type=float)
    parser.add_argument('--checkpoint_path', action="store")
    parser.add_argument('--video_path', action="store")
    #parser.add_argument('--tracker',action="store")
    parser.add_argument('--tconf',action="store",default='trackers/SiamMask/experiments/siammask_sharp/config_vot.json')
    parser.add_argument('--tmodel',action="store",default='trackers/SiamMask/experiments/siammask_sharp/SiamMask_VOT.pth')
    parser.add_argument('--det_at', action="store", default=20, type=int)
    args = parser.parse_args()
    return args


def inference_video(dataset_path, model_name, checkpoint_path, score_threshold, video_path, tconf, tmodel, det_at):
    random.seed(101)
    color_scheme={}
    classes = fetch_classes()
    detections_dict = {}
    for each_class in classes:
        if each_class.startswith('drone'):
            r,g,b = random.randint(0,255),random.randint(0,255),random.randint(0,255)
            color_scheme[each_class] = (r,g,b)
            detections_dict[each_class] = []


    #-----------------
    # Obj detection model load and setup
    num_classes = len(classes)
    model = get_model_instance(model_name, num_classes)
    checkpoint = torch.load(checkpoint_path)
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    # Obj tracking model load and setup
    cfg = load_config(SimpleNamespace(config=tconf))
    #cfg = load_config(args)
    siammask = Custom(anchors=cfg['anchors'])
    siammask = load_pretrain(siammask, tmodel)
    siammask.eval().to(device)

    lookforfirstdrone = 1
    track_objects =0
    state = np.empty(10 ,dtype=object)

    #---------------------
    cap = cv2.VideoCapture(video_path)

    if (cap.isOpened()):
      print("File opened!! ",video_path)
    else:
      print("Cannot open file ",video_path)
      return -1

    width, height = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = cap.get(cv2.CAP_PROP_FPS)
    fname = "output/output.avi"
    video_writer = cv2.VideoWriter(fname, cv2.VideoWriter_fourcc(*'XVID'), fps, (width, height))



    count = 0
    total_time = 0
    ret = True
    while(ret):
        # Capture frame-by-frame
        torch.cuda.synchronize()
        tic = time.time()
        ret, frame = cap.read()
        if not ret:
            break


        #MN
        if ((count % det_at) == 0 or lookforfirstdrone):

            #frame = cv2.resize(frame,(1280,720))
            #MN  - init flags and counters
            found_drone = 0
            d = 0

            prediction = []
            with torch.no_grad():

                transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
                image = transform(frame)

                prediction = model([image.to(device)])[0]


                length = len(prediction['scores'].tolist())
                for i in range (length):
                    score = str(round(prediction['scores'].tolist()[i],2))
                    if float(score) > score_threshold:
                        label = classes[prediction['labels'].tolist()[i]]
                        if label.startswith('drone'):
                            box = list(map(int, prediction['boxes'].tolist()[i]))
                            detections_dict[label].append(box)
                for each_key in detections_dict.keys():
                    if len(detections_dict[each_key])>1:
                        detections_dict[each_key] = non_max_suppression_fast(np.array(detections_dict[each_key]),0.3)
                    for box in detections_dict[each_key]:
                        frame = cv2.rectangle(frame,(box[0],box[1]),(box[2],box[3]),color_scheme[each_key] ,2)
                        frame = cv2.putText(frame,each_key,(box[0],box[1]-10),cv2.FONT_HERSHEY_SIMPLEX,0.5,color_scheme[each_key],2)
                        #MN
                        found_drone = 1
                        #reset state, as we want to reset the tracker
                        if d == 0:
                            state_lst = []
                        #init the tracker for every detected drone
                        x, y = int(box[0]), int(box[1])
                        w, h = int(box[2]) - x, int(box[3]) - y
                        target_pos = np.array([x + w / 2, y + h / 2])
                        target_sz = np.array([w, h])
                        print("detected drone in frame ",count)
                        state_lst.append((siamese_init(frame, target_pos, target_sz, siammask, cfg['hp'], device=device)))
                        d += 1  #count no of drones

                    detections_dict[each_key]=[]
            '''# MN
            #cv2.imshow('frame',frame)
            video_writer.write(frame)'''

        #MN : set/reset some flags
        if found_drone :
            if lookforfirstdrone :
                lookforfirstdrone = 0 # no longer looking for first
            track_objects = 1
        else : # no drone found
            track_objects = 0
            if  lookforfirstdrone == 0:
                track_objects = 1

        if track_objects :
            # how many drone objects to track?
            #print("shape of list" ,len(state_lst))
            for state in state_lst :
                state.update((siamese_track(state, frame, mask_enable=True, refine_enable=True, device=device)))
                location = state['ploygon'].flatten()
                mask = state['mask'] > state['p'].seg_thr
                frame[:, :, 2] = (mask > 0) * 255 + (mask == 0) * frame[:, :, 2]
                cv2.polylines(frame, [np.int0(location).reshape((-1, 1, 2))], True, (0, 255, 0), 3)
                #cv2.putText(frame, label, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 1)
                #print("tracked frame ", count)

        count += 1
        # only on first 1000 frames
        cv2.imshow('frame',frame)
        toc = time.time()
        total_time += (toc-tic)
        if cv2.waitKey(1) & 0xFF == ord('\x1b'):
            break
        video_writer.write(frame)
    video_writer.release()
    cap.release()
    cv2.destroyAllWindows()

    # When everything done, release the capture
    print ("fps =  %s"%(1/(total_time/count)))
    #ffmpeg -i output.avi output.mp4

def main():
    args = argument_parser()
    #siammask,cfg = SiamMask_initialize()
    inference_video(args.dataset_path,args.model_name,args.checkpoint_path,args.score_threshold,args.video_path,args.tconf,args.tmodel,args.det_at)

if __name__== "__main__":
  main()
