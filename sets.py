import os
import numpy as np
import argparse, random

def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example: \n python sets.py --split 90 5 5' )
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--split', nargs='+', type=int, default=[90,5,5])
    args = parser.parse_args()
    return args


def create_sets(root_folder,split):
    #Splitting Datasets
    filenames = os.listdir(root_folder+'/Annotations')
    filenames = [i.split('.xml', 1)[0] for i in filenames]
    split = np.array(split)
    split = split/100
    filenames.sort()  # make sure that the filenames have a fixed order before shuffling
    random.seed(230)
    random.shuffle(filenames) # shuffles the ordering of filenames (deterministic given the chosen seed)

    split_1 = int(split[0] * len(filenames))
    split_2 = int((split[0]+split[1]) * len(filenames))
    train_filenames = filenames[:split_1]
    val_filenames = filenames[split_1:split_2]
    test_filenames = filenames[split_2:]

    f= open(os.path.join(root_folder,'ImageSets','Main','train.txt'),"w+")
    for each in train_filenames:
        f.write(each+'\n')
    f.close()

    f= open(os.path.join(root_folder,'ImageSets','Main','trainval.txt'),"w+")
    for each in val_filenames:
        f.write(each+'\n')
    f.close()


    f= open(os.path.join(root_folder,'ImageSets','Main','test.txt'),"w+")
    for each in test_filenames:
        f.write(each+'\n')
    f.close()


def main():
    args = argument_parser()
    create_sets(args.dataset_path,args.split)


if __name__== "__main__":
  main()
