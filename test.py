import torchvision, torch, os
from model_instance import get_model_instance
from dataset_preparation import dataset_prep
import argparse
from xmls_to_csv import xml_to_csv
from classes import fetch_classes


def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example: \n python test.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_optimizer_sgd_classes_15_epoch_8.pth ' )
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store", default='resnet18_fpn')
    parser.add_argument('--checkpoint_path', action="store")
    args = parser.parse_args()
    return args

def test(model,data_loader,checkpoint_path):
    checkpoint = torch.load(checkpoint_path)
    start_epoch = checkpoint['epoch']
    model.load_state_dict(checkpoint['state_dict'])
    model.train()
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)
    count = 0
    total_loss = 0
    loss_classifier = 0
    loss_box_reg = 0
    loss_rpn_box_reg = 0
    loss_objectness = 0
    for images, targets in data_loader:
        count += 1
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
        with torch.no_grad():
            loss_dict = model(images, targets)
            loss_classifier += loss_dict['loss_classifier'].tolist()
            loss_box_reg += loss_dict['loss_box_reg'].tolist()
            loss_rpn_box_reg += loss_dict['loss_rpn_box_reg'].tolist()
            loss_objectness += loss_dict['loss_objectness'].tolist()
            losses = sum(loss for loss in loss_dict.values()).tolist()
            total_loss += losses
    print("\nAverage Loss per test sample\n")
    print ('\tloss_classifier : %s\n\tloss_box_reg : %s\n\tloss_rpn_box_reg : %s\n\tloss_objectness : %s\n\ttotal_loss : %s\n'%(loss_classifier/count,loss_box_reg/count,loss_rpn_box_reg/count,loss_objectness/count,total_loss/count))

def main():
    args = argument_parser()
    xml_to_csv(args.dataset_path)
    classes = fetch_classes()
    num_classes = len(classes)
    data_loader_test = dataset_prep(args.dataset_path,'test',classes)
    model = get_model_instance(args.model_name, num_classes)
    test(model,data_loader_test,args.checkpoint_path)

if __name__== "__main__":
  main()
