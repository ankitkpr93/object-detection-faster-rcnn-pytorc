def fetch_classes():
    classes = ['background', 'drone_dji-phantom-4-pro', 'drone_yuneec-typhoon-h', 'drone_3dr-solo', 'drone_parrot-anafi', \
    'drone_dji-mavic-pro', 'drone_dji-mavic-air', 'quad-bike', 'human', 'drone_parrot-mambo', 'bird', 'bee', 'vehicle', 'small-bird', 'shadows']
    return classes
