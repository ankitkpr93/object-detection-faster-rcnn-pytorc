import torch, os
import pandas as pd
from PIL import Image

class Dataset(torch.utils.data.Dataset):
    def __init__(self, root, dataset_type,classes,transforms=None):
        self.root = root
        self.transforms = transforms
        self.annotations_csv = pd.read_csv(os.path.join(self.root, "labels.csv"))
        self.classes = classes



        if dataset_type == 'train':
            self.imgs = []
            with open(root+'/ImageSets/Main/train.txt') as fp:
                for cnt, line in enumerate(fp):
                    self.imgs.append(line[:-1]+'.jpg')
        if dataset_type == 'test':
            self.imgs = []
            with open(root+'/ImageSets/Main/test.txt') as fp:
                for cnt, line in enumerate(fp):
                    self.imgs.append(line[:-1]+'.jpg')
        if dataset_type == 'val':
            self.imgs = []
            with open(root+'/ImageSets/Main/trainval.txt') as fp:
                for cnt, line in enumerate(fp):
                    self.imgs.append(line[:-1]+'.jpg')
        # load all image files, sorting them to
        # ensure that they are aligned

        #self.annotations = list(sorted(os.listdir(os.path.join(root, "Annotations"))))

    def __getitem__(self, idx):
        # load images ad masks
        img_path = os.path.join(self.root, "JPEGImages", self.imgs[idx])

        #annotations_path = os.path.join(self.root, "Annotations", self.annotations[idx])
        img = Image.open(img_path).convert("RGB")
        #classes = ['background'].extend(classes)
        # get bounding box coordinates for each mask
        boxes = []
        labels = []
        subset = self.annotations_csv[self.annotations_csv['filename']==img_path.split('/')[-1].split('.')[0]+'.xml']
        for index,row in subset.iterrows():
            xmin = row['xmin']
            xmax = row['xmax']
            ymin = row['ymin']
            ymax = row['ymax']
            boxes.append([xmin, ymin, xmax, ymax])
            labels.append(self.classes.index(row['class']))
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        image_id = torch.tensor([idx])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["image_id"] = image_id
        #target["area"] = area

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.imgs)
