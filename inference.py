import random, cv2, torch, torchvision, os, time
from model_instance import get_model_instance
import pandas as pd
import numpy as np
import argparse
from nms import non_max_suppression_fast
from classes import fetch_classes
from skimage.feature import hog
from skimage import feature


def argument_parser():
    parser = argparse.ArgumentParser(description='Provide the necessary arguments. See the example below', epilog='Usage example:\
     \n python inference.py --model_name resnet18_fpn --checkpoint_path checkpoints/drone_resnet18_fpn_classes_15_epoch_5.pth --video_path videos/2019-09-25_1535.mp4 --score_threshold 0.9')
    parser.add_argument('--dataset_path', action="store", default='dataset/VOC2007')
    parser.add_argument('--model_name', action="store")
    parser.add_argument('--score_threshold', action="store", default=0.8, type=float)
    parser.add_argument('--checkpoint_path', action="store")
    parser.add_argument('--video_path', action="store")
    parser.add_argument('--output_path', action="store",default="output/output.avi")
    parser.add_argument('--detection_interval', action="store", default=1, type=int)
    parser.add_argument('--image_path',action="store")
    args = parser.parse_args()
    return args

def inference_video(dataset_path, model_name, checkpoint_path, score_threshold, video_path, output_vid, interval, image_path):

    random.seed(101)
    color_scheme={}
    classes = fetch_classes()
    detections_dict = {}
    for each_class in classes:
        r,g,b = random.randint(0,255),random.randint(0,255),random.randint(0,255)
        color_scheme[each_class] = (r,g,b)
        detections_dict[each_class] = []


    num_classes = len(classes)
    model = get_model_instance(model_name, num_classes)
    checkpoint = torch.load(checkpoint_path)
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    if (image_path is not None):
        image = cv2.imread(image_path)
        #print (image)
        torch.cuda.synchronize()
        transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
        frame = transform(image)
        prediction = model([frame.to(device)])[0]
        length = len(prediction['scores'].tolist())
        for i in range (length):
            score = str(round(prediction['scores'].tolist()[i],2))
            if float(score) > score_threshold:
                label = classes[prediction['labels'].tolist()[i]]
                detections_dict[label] = list(detections_dict[label])
                box = list(map(int, prediction['boxes'].tolist()[i]))
                    
                detections_dict[label].append(box)
            for each_key in detections_dict.keys():
                if len(detections_dict[each_key])>1:
                    detections_dict[each_key] = non_max_suppression_fast(np.array(detections_dict[each_key]),0.3)
                for box in detections_dict[each_key]:
                    frame = cv2.rectangle(image,(box[0],box[1]),(box[2],box[3]),color_scheme[each_key] ,2)
                    frame = cv2.putText(image,each_key,(box[0],box[1]-10),cv2.FONT_HERSHEY_SIMPLEX,0.5,color_scheme[each_key],2)
        cv2.imwrite('output/detections.jpg',image)
                        

    
    if(video_path is not None):
        cap = cv2.VideoCapture(video_path)

        video_writer = None
        if (output_vid is not None):
            width, height = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            fps = cap.get(cv2.CAP_PROP_FPS)
            video_writer = cv2.VideoWriter('output/output_%s'%(video_path.split("/")[-1]), cv2.VideoWriter_fourcc(*'MJPG'), fps, (1280, 720))

        count = 0
        total_time = 0
        ret = True
        while(ret):
            # Capture frame-by-frame
            torch.cuda.synchronize()
            tic = time.time()
            ret, frame = cap.read()
            if not ret:
                break
            frame = cv2.resize(frame,(1280,720))
            if (count%interval==0):
                prediction = []
                with torch.no_grad():
                    transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
                    image = transform(frame)

                    prediction = model([image.to(device)])[0]


                    length = len(prediction['scores'].tolist())
                    for i in range (length):
                        score = str(round(prediction['scores'].tolist()[i],2))
                        if float(score) > score_threshold:
                            label = classes[prediction['labels'].tolist()[i]]
                            if label.startswith('drone'):
                                box = list(map(int, prediction['boxes'].tolist()[i]))
                                detections_dict[label].append(box)
                    for each_key in detections_dict.keys():
                        if len(detections_dict[each_key])>1:
                            detections_dict[each_key] = non_max_suppression_fast(np.array(detections_dict[each_key]),0.3)
                        for box in detections_dict[each_key]:
                            frame = cv2.rectangle(frame,(box[0],box[1]),(box[2],box[3]),color_scheme[each_key] ,2)
                            frame = cv2.putText(frame,each_key,(box[0],box[1]-10),cv2.FONT_HERSHEY_SIMPLEX,0.5,color_scheme[each_key],2)
                            #cv2.imwrite('/home/ankit_kapoor/Downloads/frames/frame_%s.jpg'%count,frame)
                        detections_dict[each_key]=[]

            if (output_vid is None):
                cv2.imshow('frame',frame)
            else:
                video_writer.write(frame)

            toc = time.time()
            total_time += (toc-tic)
            if cv2.waitKey(1) & 0xFF == ord('\x1b'):
                break
            count += 1


        if (video_writer is not None):
            video_writer.release()

        cap.release()
        cv2.destroyAllWindows()

        # When everything done, release the capture
        print ("fps =  %s"%(1/(total_time/count)))

def main():
    args = argument_parser()

    inference_video(args.dataset_path,args.model_name,args.checkpoint_path,args.score_threshold,args.video_path, args.output_path, args.detection_interval, args.image_path)

if __name__== "__main__":
  main()
